/* -*- c++ -*- */

#ifndef _COMMON_H
#define _COMMON_H

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <SDL.h>
#include <SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <sys/time.h>

#include <iostream>
#include <vector>
#include <string>

using namespace std;

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define FOREACH(itr,vect) for (typeof((vect).begin()) itr = (vect).begin(); itr != (vect).end(); itr++)

class RefCount {
    int refcount;
    //WeakRefBase* weak;
    //friend class WeakRefBase;
public:
    RefCount() : refcount(1) { }
    void MakeStatic() { refcount=-1; }
    void Ref() { if (refcount!=-1) refcount++; }
    void Deref() { if (refcount==-1) return; refcount--; if (!refcount) delete this; }
    virtual ~RefCount() {
        /*WeakRefBase* cur = weak;
        while (cur) {
            //cout << "clearing weakref "<<cur<<endl;
            cur->rref = NULL;
            cur = cur->next;
            }*/
        if (refcount>0) {
            //destructor_breakpoint();
            cerr << "Foo! deleting object with references!"<<endl;
        }
    }

};

#endif
