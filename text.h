/* -*- c++ -*- */

#ifndef _TEXT_H
#define _TEXT_H

#include "common.h"
#include "util.h"

#include <iostream>
#include <sstream>

#define GLYPH_VALID 1

struct Color {

    double r, g, b, a;
    Color(double r=1.0,double g=1.0,double b=1.0,double a=1.0) : r(r), g(g), b(b), a(a) { }
    Color(int color) :
        r(((color >> 16) & 255) / (double)255.0),
        g(((color >> 8) & 255) / (double)255.0),
        b(((color) & 255) / (double)255.0),
        a(((color >> 24) & 255) / (double)255.0) { }

    void Set() {
        glColor4d(r, g, b, a);
    }
};

struct Glyph {
    short ox, oy, adv;
    int sx, sy;
    short sw, sh;
};

class Font {
public:
    int tex, tw, th;
    int ascent, descent;

    Glyph* glyphs;
    int nglyphs;

    unsigned char* imgdat;
    int imgdatlen;

    Font(unsigned char* imgdat, int imgdatlen, Glyph* glyphs,
         int nglyphs, int ascent, int descent);

    int RenderText(const char* txt, int sx, int sy);
    int MeasureText(const char* txt, int* pminx, int* pmaxx, int* pminy, int* pmaxy) const;

    static Font* default_font;
};

struct TextChunk {
    int tab;
    string txt;
    Font* font;
    Color color;

    TextChunk(string txt, Font* font, Color color, int tab) :
        tab(tab), txt(txt), font(font), color(color) { }

};

struct TextLine {
    vector<TextChunk> chunks;
    int ascent, descent;

    TextLine() : ascent(0), descent(0) { }
};

class TextLayout {
    vector<TextLine> lines;

    Font* cfont;
    Color ccolor;

    ostringstream strm;

    int horiz_tab;

    void addchunk(const string& txt, Font* font, Color color);
    void splitlines(const string& txt, Font* font, Color color);
    void checktext();

public:
    TextLayout(Font* font=Font::default_font, Color color=Color());

    void Render(int bx, int by);
    void Measure(int* pminx, int* pmaxx, int* pminy, int* pmaxy);

    TextLayout& add(const string& txt, Font* font, Color color) {
        checktext();
        splitlines(txt, cfont, color);
        return *this;
    }
    TextLayout& add(const string& txt, Color color) {
        return add(txt, cfont, color);
    }
    TextLayout& add(const string& txt, Font* font) {
        return add(txt, font, ccolor);
    }
    TextLayout& add(const string& txt) {
        return add(txt, cfont, ccolor);
    }
    Font* font() {
        return cfont;
    }
    Color color() {
        return ccolor;
    }
    TextLayout& settab(int tab) {
        checktext();
        horiz_tab = tab;
        return *this;
    }
    TextLayout& color(const Color color) {
        checktext();
        ccolor = color;
        return *this;
    }
    TextLayout& color(double r, double g, double b, double a=1.0) {
        checktext();
        ccolor = Color(r, g, b, a);
        return *this;
    }
    TextLayout& color(int color) {
        checktext();
        ccolor = Color(color);
        return *this;
    }
    TextLayout& font(Font* font) {
        checktext();
        cfont = font;
        return *this;
    }

    ostream& stream() {
        return strm;
    }
};

struct htab {
    int tab;
    htab(int tab) : tab(tab) { }
};

template<typename T>
static inline TextLayout& operator<<(TextLayout& tl, const T& v) {
    tl.stream() << v;
    return tl;
}

static inline TextLayout& operator<<(TextLayout& tl, ostream& (*pf)(ostream&)) {
    pf(tl.stream());
    return tl;
}

template<>
inline TextLayout& operator<<(TextLayout& tl, const Color& col) {
    return tl.color(col);
}

static inline TextLayout& operator<<(TextLayout& tl, Font& font) {
    return tl.font(&font);
}

static inline TextLayout& operator<<(TextLayout& tl, const htab t) {
    return tl.settab(t.tab);
}


#endif
