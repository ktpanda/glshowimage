
#include "config.h"

#include "common.h"
#include "util.h"

SDL_Window* mainwindow;
SDL_GLContext maincontext;
int screenw;
int screenh;
int real_screenw;
int real_screenh;
int is_fullscreen;
int rotation;
double screenasp;

void _checkerror (const char* file, int line) {
    GLenum errCode;

    if ((errCode = glGetError ()) != GL_NO_ERROR) {
        const GLubyte *errString;
        errString = gluErrorString (errCode);
        fprintf (stderr, "OpenGL error detected at %s:%d: %s\n", file, line, errString);
        SDL_Quit ();
        exit (1);
    }
}

void AllocTexture(int w, int h, int flags) {
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, (flags & FL_REPX) ? GL_REPEAT:GL_CLAMP_TO_EDGE);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, (flags & FL_REPY) ? GL_REPEAT:GL_CLAMP_TO_EDGE);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (flags & FL_MIPS)?GL_LINEAR_MIPMAP_LINEAR:GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,0, GL_RGBA, w, h, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, NULL);
    CHECKERROR();
}
void ReplaceTexture(void* pixels, int w, int h, int format) {
    glPixelStorei (GL_UNPACK_SKIP_PIXELS, 0);
    glPixelStorei (GL_UNPACK_SKIP_ROWS, 0);
    glPixelStorei (GL_UNPACK_ALIGNMENT, 1);

    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, format, GL_UNSIGNED_BYTE, pixels);
    CHECKERROR();
}

void UploadSurfaceToTexture (SDL_Surface * surf) {
    SDL_Surface *conv = NULL;
    SDL_PixelFormat* f = surf->format;
#define pack(a,b,c,d) (((a)<<24) | ((b)<<16) | ((c)<<8) | (d))

    Uint32 ssig = pack(f->Ashift, f->Rshift, f->Gshift, f->Bshift);


    int format = 0;
    if (f->BytesPerPixel == 4) {

        switch (ssig) {
        case pack(24, 0, 8, 16):
            format = GL_RGBA;
            break;
        case pack(24, 16, 8, 0):
            format = GL_BGRA;
            break;
        }
    } else if (f->BytesPerPixel == 3) {
        switch (ssig) {
        case pack(0, 0, 8, 16):
            format = GL_RGB;
            break;
        case pack(0, 16, 8, 0):
            format = GL_BGR;
            break;
        }
    }

    int align = 0;
    int expectpitch = surf->w*f->BytesPerPixel;

    if (expectpitch == surf->pitch) align = 1;
    else if (((expectpitch+1)&~1) == surf->pitch) align = 2;
    else if (((expectpitch+3)&~3) == surf->pitch) align = 4;
    //else if (((expectpitch+7)&~7) == surf->pitch) align = 8;

    //fprintf(stderr,"%d %d %d %d %d\n",align, format, surf->pitch, surf->w, expectpitch);
    if (format == 0 || align == 0) {
        conv =
            SDL_CreateRGBSurface (SDL_SWSURFACE, surf->w, surf->h, 32, 0x000000FF,
                                  0x0000FF00, 0x00FF0000, 0xFF000000);
        SDL_BlitSurface (surf, NULL, conv, NULL);

        surf = conv;
        format = GL_RGBA;
        align = 1;
    }
    glPixelStorei (GL_UNPACK_SKIP_PIXELS, 0);
    glPixelStorei (GL_UNPACK_SKIP_ROWS, 0);
    glPixelStorei (GL_UNPACK_ALIGNMENT, align);

    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, surf->w, surf->h,
                    format, GL_UNSIGNED_BYTE, surf->pixels);
    CHECKERROR();

    if (conv) SDL_FreeSurface (conv);
}

void DrawRect(double x1, double y1, double x2, double y2) {
    glBegin(GL_QUADS);
    glVertex2d(x1,y1); glVertex2d(x1,y2);
    glVertex2d(x2,y2); glVertex2d(x2,y1);
    glEnd();
    CHECKERROR();
}

void InitGL(int w, int h, int fs) {

    SDL_GL_SetAttribute (SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute (SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute (SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute (SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute (SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute (SDL_GL_STENCIL_SIZE, 0);
    SDL_GL_SetAttribute (SDL_GL_ACCUM_RED_SIZE, 0);
    SDL_GL_SetAttribute (SDL_GL_ACCUM_GREEN_SIZE, 0);
    SDL_GL_SetAttribute (SDL_GL_ACCUM_BLUE_SIZE, 0);
    SDL_GL_SetAttribute (SDL_GL_ACCUM_ALPHA_SIZE, 0);

    mainwindow = SDL_CreateWindow("glshowimage",
                                  SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                  w, h,
                                  SDL_WINDOW_OPENGL | (fs ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0));

    if (!mainwindow) return;
    maincontext = SDL_GL_CreateContext(mainwindow);

    if (!maincontext) {
        SDL_DestroyWindow(mainwindow);
        mainwindow = NULL;
        return;
    }
    is_fullscreen = fs;
    SDL_ShowCursor (0);

    int realr, realg, realb, realz, reald;

    SDL_GL_GetAttribute (SDL_GL_RED_SIZE, &realr);
    SDL_GL_GetAttribute (SDL_GL_GREEN_SIZE, &realg);
    SDL_GL_GetAttribute (SDL_GL_BLUE_SIZE, &realb);
    SDL_GL_GetAttribute (SDL_GL_DEPTH_SIZE, &realz);
    SDL_GL_GetAttribute (SDL_GL_DOUBLEBUFFER, &reald);

    glDisable        (GL_POLYGON_SMOOTH);
    glEnable    (GL_TEXTURE_2D);
    glEnable    (GL_BLEND);
    glDisable   (GL_ALPHA_TEST);
    glDisable   (GL_DEPTH_TEST);
    glDisable   (GL_FOG);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDrawBuffer(GL_BACK);
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT);
    SDL_GL_SwapWindow(mainwindow);
    SDL_GetWindowSize(mainwindow, &real_screenw, &real_screenh);
    Rotate(0);

    CHECKERROR();
}

void Rotate(int r) {
    rotation = r;
    if (rotation & 1) {
        screenw = real_screenh;
        screenh = real_screenw;
    } else {
        screenw = real_screenw;
        screenh = real_screenh;
    }

    screenasp = ((double)screenh)/screenw;
}

void RotateOrtho(double left, double right, double bottom, double top) {
    double xleft = left, xright = right, xbottom = bottom, xtop = top;
    if (rotation & 1) {
        double t;
        xleft = bottom;
        xright = top;
        xbottom = left;
        xtop = right;
    }
    glOrtho(xleft, xright, xbottom, xtop, -1, 1);
    /*switch(rotation) {
    case 0: glOrtho(left, right, bottom, top, -1, 1); break;
    case 1: glOrtho(bottom, top, right, left, -1, 1); break;
    case 2: glOrtho(right, left, top, bottom, -1, 1); break;
    case 3: glOrtho(top, bottom, left, right, -1, 1); break;
    }*/
    glTranslated((xleft + xright) / 2, (xtop + xbottom) / 2, 0);
    glRotated(90 * rotation, 0, 0, -1);
    glTranslated(-(left + right) / 2, -(top + bottom) / 2, 0);
}
