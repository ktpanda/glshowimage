/* -*- c++ -*- */

#ifndef _UTIL_H
#define _UTIL_H

#define FL_REPX 1
#define FL_REPY 2
#define FL_MIPS 4

extern SDL_Window* mainwindow;
extern SDL_GLContext maincontext;
extern int screenw, screenh;
extern int real_screenw, real_screenh;
extern int is_fullscreen;
extern int rotation;
extern double screenasp;

static inline int powerof2 (int in)
{
    int i = 0;
    in--;
    while (in) {
        in >>= 1;
        i++;
    }
    return 1 << i;
}

#define CHECKERROR() _checkerror(__FILE__, __LINE__)

void _checkerror(const char* file, int line);

void AllocTexture(int w, int h, int flags);

void ReplaceTexture(void* pixels, int w, int h, int format);

void UploadSurfaceToTexture (SDL_Surface * surf);

void DrawRect(double x1, double y1, double x2, double y2);

void InitGL(int w, int h, int fs);

void RotateOrtho(double left, double right, double bottom, double top);
void Rotate(int r);

#endif
