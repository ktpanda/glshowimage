/*
    showimage:  A test application for the SDL image loading library.
    Copyright (C) 1999, 2000, 2001  Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Sam Lantinga
    slouken@libsdl.org
*/

#include "config.h"

#include "common.h"
#include "util.h"
#include "text.h"

#include <map>
#include <algorithm>

#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

int pictex;
int maxsize;

extern Font font_default, font_default24;

inline void chomp(char* str) {
    int l=strlen(str);
    while (--l>=0 && (str[l]=='\n' || str[l]=='\r')) str[l]=0;
}

class Image {
public:
    string name;
    string filename;
    bool select;
    int w, h;


    Image(string name="") : name(name), filename(name), select(false), w(0), h(0) { }
    Image(string name, string filename) : name(name), filename(filename), select(false), w(0), h(0) { }

    bool Load() {
        Uint32 begin = SDL_GetTicks();
        if (w > maxsize || h > maxsize) {
            cerr << name << ": too big (cached)"<<endl;
            return false;
        }

        SDL_Surface* image = IMG_Load(filename.c_str());

        Uint32 end1 = SDL_GetTicks();
        if (!image) {
            cerr << name << ": "<<SDL_GetError()<<endl;
            return false;
        }

        w = image->w;
        h = image->h;

        if (image->w > maxsize || image->h > maxsize) {
            cerr << name << ": too big"<<endl;
            SDL_FreeSurface (image);
            return false;
        }
        SDL_SetWindowTitle(mainwindow, name.c_str());

        UploadSurfaceToTexture(image);

        SDL_FreeSurface (image);

        Uint32 end2 = SDL_GetTicks();

        //fprintf(stderr,"%d %d\n",end1-begin,end2-end1);
        return true;
    }

};

class Component : public RefCount {
public:
    Component* parent;
    vector<Component*> children;
    int x, y;
    bool canfocus, isfocused;

    Component(Component* parent=NULL) : parent(parent), canfocus(false), isfocused(false), x(0), y(0) {
    }

    virtual ~Component() {
        FOREACH(itr, children) {
            (*itr)->Deref();
        }
    }

    void Add(Component* c) {
        if (c->parent) return;
        children.push_back(c);
        c->parent = this;
        c->Ref();
    }
    void Remove(Component* c) {
        if (c->isfocused) SwitchFocus();
        FOREACH(itr, children) {
            if (*itr == c) {
                children.erase(itr);
                c->parent = NULL;
                c->Deref();
                break;
            }
        }
    }

    void Render(int x, int y) {
        Draw(x, y);
        FOREACH(c, children) {
            (*c)->Render(x + (*c)->x, y + (*c)->y);
        }
    }
    void GrabFocus() {
        SetFocus(this);
    }
    virtual void SetFocus(Component* c) {
        if (parent) parent->SetFocus(c);
    }
    virtual void SwitchFocus() {
        if (parent) parent->SwitchFocus();
    }
    virtual void Refresh() {
        if (parent) parent->Refresh();
    }
    virtual void Draw(int x, int y) { }
    virtual bool Event(SDL_Event& evt) { return false; }
    virtual void Idle() { }

};

enum Key {
    K_STEP_F1, K_STEP_B1,
    K_STEP_F5, K_STEP_B5,
    K_SELECT, K_JUMP_SELECT,
    K_FAST,
    K_LAST
};

enum KeyState {
    S_RELEASED,
    S_PRESSED,
    S_LOCKED
};

class TextEntry {
    string inputb, inpute;

    int curs, len;
    int skip;
    int defer_keysym;

    void split();
    void join();
    void check_deferred_keysym();
    Font* font;

public:
    bool done;
    bool accept;

    TextEntry(Font* font_=NULL) {
        font = font_;
        if (!font)
            font = Font::default_font;
        curs = len = 0;
        skip = 0;
        done = accept = false;
        defer_keysym = 0;
    }
    ~TextEntry() {
    }

    void settxt(string s);
    string gettxt();
    void insert(char c);
    void insert(const string& s);
    void delete_back(int num);
    void delete_fwd(int num);
    int draw(int x, int y);
    void clear();
    bool process_input(SDL_Event& evt);
};


void TextEntry::split() {
    int diff = curs - inputb.size();
    if (diff < 0) {
        inpute.insert(0, inputb, curs, string::npos);
        inputb.resize(curs);
    } else if (diff > 0) {
        inputb.append(inpute, 0, diff);
        inpute.erase(0, diff);

    }
}
void TextEntry::join() {
    if (inpute.empty())
        return;

    inputb.append(inpute);
    inpute.clear();
}

void TextEntry::settxt(string s) {
    inputb = s;
    inpute.clear();
    curs = len = inputb.size();
    skip = 0;
}
string TextEntry::gettxt() {
    join();
    return inputb;
}

void TextEntry::insert(char c) {
    split();
    inputb.append(1, c);
    curs++; len++;
}

void TextEntry::insert(const string& s) {
    split();
    inputb.append(s);
    curs++; len++;
}

void TextEntry::delete_back(int num) {
    split();

    curs -= num;
    len -= num;

    if (curs < 0) {
        len -= curs;
        curs = 0;
    }
    inputb.resize(curs);
}

void TextEntry::delete_fwd(int num) {
    curs += num;
    int diff = len - curs;
    if (diff < 0) {
        num += diff;
        curs = len;
    }

    delete_back(num);
}

int TextEntry::draw(int x, int y) {
    //skip = bound(skip, curs - (TXTW - x), curs);

    //Uint8* pos = (Uint8*)t.surf->pixels + pitch * (y * 14);
    /*if (skip) {
        Uint8* tp = pos + x * 8;
        for (int j = 1; j < 4; j++)
            tp[j] = 1;

        for (int j = 0; j < 13; j++, tp += pitch)
            tp[0] = 1;

        for (int j = 0; j < 4; j++)
            tp[j] = 1;

            }*/

    split();

    int curspos = x;
    int tx = x;
    const char* str_data = inputb.data() + skip;
    int slen = inputb.size() - skip;

    if (slen > 0) {
        int adv = font->MeasureText(str_data, NULL, NULL, NULL, NULL);
        font->RenderText(str_data, tx, y);
        tx += adv;

        slen = 0;
    }
    curspos = tx;


    str_data = inpute.data() - slen;
    slen += inpute.size();
    if (slen > 0) {
        int adv = font->MeasureText(str_data, NULL, NULL, NULL, NULL);
        font->RenderText(str_data, tx, y);
        tx += adv;
    }

    return curspos;
}

//#define tdb(x) x
#define tdb(x) (void)0

void TextEntry::clear() {
    inputb.clear();
    inpute.clear();
    curs = len = 0;
}

template<typename T> inline T bound(T v, T min, T max) {
    if (v < min) return min;
    if (v > max) return max;
    return v;
}

bool TextEntry::process_input(SDL_Event& event) {
    bool val = true;
    bool istext = false;
    int uni, sym;
    bool ctrl;
    SDL_Event nextevent;
    int havenext;
    switch (event.type) {
        case SDL_KEYDOWN:
            havenext = SDL_PeepEvents(&nextevent, 1, SDL_PEEKEVENT, SDL_FIRSTEVENT, SDL_LASTEVENT);
            if (havenext && nextevent.type == SDL_TEXTINPUT)
                istext = true;
            sym = event.key.keysym.sym;
            ctrl = (event.key.keysym.mod & KMOD_CTRL) != 0;
            tdb(cout << "key " << sym << " hnext " << havenext << " istext " << istext <<endl);
            if (ctrl) {
                switch (sym) {
                    case SDLK_a:
                        curs = 0;
                        return true;

                    case SDLK_e:
                        curs = len;
                        return true;

                    case SDLK_c:
                        istext = 0;
                        if (len)
                            clear();
                        else
                            done = true;

                        return true;
                }
            } else {
                switch (sym) {
                    case SDLK_ESCAPE:
                        istext = 0;
                        if (len)
                            clear();
                        else
                            done = true;

                        return true;

                    case SDLK_KP_ENTER:
                    case SDLK_RETURN:
                        done = true;
                        accept = true;
                        return true;

                    case SDLK_LEFT:
                    case SDLK_KP_4:
                        if (!istext)
                            curs = bound(curs - 1, 0, len);
                        return true;

                    case SDLK_RIGHT:
                    case SDLK_KP_6:
                        if (!istext)
                            curs = bound(curs + 1, 0, len);
                        return true;

                    case SDLK_UP:
                    case SDLK_KP_8:
                        return true;

                    case SDLK_DOWN:
                    case SDLK_KP_2:
                            return true;

                    case SDLK_HOME:
                    case SDLK_KP_7:
                        if (!istext)
                            curs = 0;
                        return true;

                    case SDLK_END:
                    case SDLK_KP_1:
                        if (!istext)
                            curs = len;
                        return true;

                    case SDLK_BACKSPACE:
                        delete_back(1);
                        return true;

                    case SDLK_DELETE:
                    case SDLK_KP_PERIOD:
                        if (!istext)
                            delete_fwd(1);
                        return true;

                }
            }
            return true;
        case SDL_TEXTINPUT: {
            char* txt = event.text.text;
            while (*txt) insert(*txt++);
            return true;
        }
    }
    return false;
}


class ImageComponent : public Component {
    vector<Image>& imglist;
    int curimg;
    int newimg;
    int bgimg;

    KeyState keys[K_LAST];
    int selectval;

    bool valid;
    bool reload;
    bool pick_imm;
    bool loop;
    bool showinfo;

    TextEntry *txtent;

public:
    ImageComponent(vector<Image>& imglist, int firstimg, bool pick_imm, bool loop, bool showinfo) :
        imglist(imglist), newimg(firstimg), valid(false), pick_imm(pick_imm), loop(loop), showinfo(showinfo) {
        for (int i = 0; i < K_LAST; i++)
            keys[i] = S_RELEASED;

        curimg = -1;
        reload = true;
        txtent = NULL;
        // Set up background image

        static const Uint8 bgdata[]={0x66,0x66,0x66, 0x99,0x99,0x99,
                                     0x99,0x99,0x99, 0x66,0x66,0x66};

        glGenTextures(1,(GLuint*)&bgimg);

        glBindTexture(GL_TEXTURE_2D,bgimg);
        glPixelStorei (GL_UNPACK_SKIP_PIXELS, 0);
        glPixelStorei (GL_UNPACK_SKIP_ROWS, 0);
        glPixelStorei (GL_UNPACK_ALIGNMENT, 1);
        CHECKERROR();

        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        glTexImage2D (GL_TEXTURE_2D,0, GL_RGB, 2, 2, 0, GL_RGB,
                      GL_UNSIGNED_BYTE, bgdata);



    }

    virtual void Draw(int x, int y) {
        if (reload) {
            curimg = newimg;
            glBindTexture(GL_TEXTURE_2D, pictex);
            valid = imglist[curimg].Load();
            reload = false;
        }
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();
        RotateOrtho (-1.0, 1.0, screenasp, -screenasp);
        if (valid) {
            double tcx = ((double)imglist[curimg].w) / maxsize;
            double tcy = ((double)imglist[curimg].h) / maxsize;
            double picw=1.0,pich=1.0;
            double picasp=((double)imglist[curimg].h)/((double)imglist[curimg].w);
            if (picasp>screenasp) {
                picw=(screenasp/picasp);
                pich=screenasp;
            } else {
                pich=(picasp);
                picw=1.0;
            }



            glColor3f(1,1,1);
            glBindTexture(GL_TEXTURE_2D,bgimg);
            glBegin(GL_QUADS);
            glTexCoord2f(0,0);
            glVertex2f(-picw,-pich);
            glTexCoord2f(picw*32,0);
            glVertex2f(picw,-pich);
            glTexCoord2f(picw*32,pich*32);
            glVertex2f(picw,pich);
            glTexCoord2f(0,pich*32);
            glVertex2f(-picw,pich);
            glEnd();

            CHECKERROR();
            glBindTexture(GL_TEXTURE_2D,pictex);
            glBegin(GL_QUADS);
            glTexCoord2f(0,0);
            glVertex2f(-picw,-pich);
            glTexCoord2f(tcx,0);
            glVertex2f(picw,-pich);
            glTexCoord2f(tcx,tcy);
            glVertex2f(picw,pich);
            glTexCoord2f(0,tcy);
            glVertex2f(-picw,pich);
            glEnd();
            CHECKERROR();
        } else {
            glDisable(GL_TEXTURE_2D);
            glColor4f(1,0,0,1);
            glLineWidth(5);
            glBegin(GL_LINES);
            glVertex2d(-1.0,-screenasp);
            glVertex2d(1.0,screenasp);
            glVertex2d(-1.0,screenasp);
            glVertex2d(1.0,-screenasp);
            glEnd();
            glEnable(GL_TEXTURE_2D);
            CHECKERROR();
        }
        if (imglist[curimg].select) {
            int z;

            glDisable(GL_TEXTURE_2D);
#define UNIT (1.0/256.0)

            for (z=0;z<2;z++) {
                if (z) {
                    glColor4f(1,1,1,1);
                    glLineWidth(3);
                } else {
                    glColor4f(0,0,0,1);
                    glLineWidth(6);
                }
                glBegin(GL_LINES);
                glVertex2d(-1.0+UNIT,-screenasp+(UNIT*16));
                glVertex2d(-1.0+(UNIT*5),-screenasp+(UNIT*20));
                glVertex2d(-1.0+(UNIT*5),-screenasp+(UNIT*20));
                glVertex2d(-1.0+(UNIT*13),-screenasp+(UNIT*12));
                glEnd();
            }
            glEnable(GL_TEXTURE_2D);
            CHECKERROR();
        }
        if (newimg != curimg) {
            glDisable(GL_TEXTURE_2D);

            for (int z = 0; z < 2; z++) {
                if (z) {
                    glColor4f(1,1,1,1);
                    glPointSize(5);
                } else {
                    glColor4f(0,0,0,1);
                    glPointSize(9);
                }
                glBegin(GL_POINTS);
                glVertex2d(-1.0+(UNIT*2),-screenasp+(UNIT*26));
                glVertex2d(-1.0+(UNIT*8),-screenasp+(UNIT*26));
                glVertex2d(-1.0+(UNIT*14),-screenasp+(UNIT*26));
                glEnd();
            }
            glEnable(GL_TEXTURE_2D);

            reload = true;
            Refresh();
        }


        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        if (showinfo) {
            Image& cimg(imglist[curimg]);
            TextLayout tl;
            tl << (curimg + 1) << "/" << imglist.size() << ": " << cimg.name << " (" << cimg.w << 'x' << cimg.h << ")";
            int tminx, tminy, tmaxx, tmaxy;

            tl.Measure(&tminx, &tmaxx, &tminy, &tmaxy);

            glDisable(GL_TEXTURE_2D);
            glEnable(GL_BLEND);
            glColor4d(0, 0, 0, 0.5);
            DrawRect(0, 0, tmaxx - tminx+10, tmaxy - tminy+10);
            glEnable(GL_TEXTURE_2D);

            tl.Render(-tminx+5, -tminy+5);
        }
        if (txtent) {
            int sx = 20;
            int sy = screenh - 40;

            glDisable(GL_TEXTURE_2D);
            glEnable(GL_BLEND);
            glColor4d(0, 0, 0, 0.5);
            DrawRect(0, sy, screenw, screenh);
            glColor4f(1, 1, 1, 1);

            glEnable(GL_TEXTURE_2D);
            int cursorpos = txtent->draw(sx, sy + 25);
            glDisable(GL_TEXTURE_2D);

            glLineWidth(1);
            glBegin(GL_LINES);
            glVertex2d(cursorpos, sy);
            glVertex2d(cursorpos, screenh);
            glEnd();

            glLineWidth(3);
            glBegin(GL_LINES);
            glVertex2d(0, sy);
            glVertex2d(sx - 4, (sy + screenh)/2);
            glVertex2d(sx - 4, (sy + screenh)/2);
            glVertex2d(0, screenh);
            glEnd();
            glEnable(GL_TEXTURE_2D);
        }
        //CheckKeys();
    }
    bool GetKey(Key k, bool ignorelock=false) {
        if (ignorelock) return keys[k] != S_RELEASED;
        return keys[k] == S_PRESSED;
    }
    void ChangeImage(int dir, int step) {
        if (keys[K_JUMP_SELECT]) {
            int j = curimg;
            newimg = curimg;
            for (int i = 0; i < step;) {
                newimg += dir;
                if (newimg < 0 || newimg >= imglist.size()) {
                    if (loop) {
                        newimg = (newimg + imglist.size()) % imglist.size();
                    } else {
                        newimg = j;
                        break;
                    }
                }
                if (imglist[newimg].select) {
                    j = newimg;
                    i++;
                }
            }
        } else {
            newimg = curimg + dir*step;
            if (loop) {
                newimg = (newimg + imglist.size()) % imglist.size();
            } else {
                if (newimg < 0) newimg = 0;
                if (newimg >= imglist.size()) newimg = imglist.size()-1;
            }
        }
        if (newimg != curimg) Refresh();
    }

    void CheckKeys() {
        if (txtent) return;
        int dir_1 = (int)GetKey(K_STEP_F1, keys[K_FAST]) - (int)GetKey(K_STEP_B1, keys[K_FAST]);
        int dir_5 = (int)GetKey(K_STEP_F5, keys[K_FAST]) - (int)GetKey(K_STEP_B5, keys[K_FAST]);

        if (dir_5) {
            ChangeImage(dir_5, 5);
        } else if (dir_1) {
            ChangeImage(dir_1, 1);
        }

        if (keys[K_SELECT]) {
            if (imglist[curimg].select ^ selectval) {
                imglist[curimg].select = selectval;
                Refresh();
            }

        }
        for (int i = 0; i < K_LAST; i++) {
            if (keys[i] == S_PRESSED) keys[i] = S_LOCKED;
        }
    }
    virtual void Idle() {
        CheckKeys();
    }

    void BeginTextEntry() {
        BeginTextEntry("");
    }

    void BeginTextEntry(string text) {
        if (!txtent) {
            Image& cimg(imglist[curimg]);
            /*printf("%s\n", cimg.name.c_str());
              fflush(stdout);*/
            txtent = new TextEntry(&font_default24);
            txtent->settxt(text);
            SDL_StartTextInput();
            Refresh();
        }
    }

    bool EnteringText() {
        return txtent != NULL;
    }

    virtual bool Event(SDL_Event& event) {
        KeyState state;
        if (txtent) {
            bool r = txtent->process_input(event);
            if (txtent->done) {
                SDL_StopTextInput();
                if (txtent->accept && curimg >= 0 && curimg < imglist.size()) {
                    Image& cimg(imglist[curimg]);
                    string s = txtent->gettxt();
                    printf("%s\t%s\n", cimg.name.c_str(), s.c_str());
                    fflush(stdout);
                }
                delete txtent;
                txtent = NULL;
            }

            if (r) {
                Refresh();
                return true;
            }
        }
        switch (event.type) {
            case SDL_WINDOWEVENT:
                switch (event.window.event) {
                    case SDL_WINDOWEVENT_EXPOSED:
                        Refresh();
                        break;
                    default:
                        break;
                }
        case SDL_KEYDOWN:
        case SDL_KEYUP:

            state = (event.type==SDL_KEYDOWN) ? S_PRESSED : S_RELEASED;
            switch (event.key.keysym.sym) {
            case SDLK_KP_4:
            case SDLK_LEFT:
            case SDLK_s:
                keys[K_STEP_B1] = state;
                break;
            case SDLK_KP_6:
            case SDLK_RIGHT:
            case SDLK_f:
                keys[K_STEP_F1] = state;
                break;
            case SDLK_KP_8:
            case SDLK_UP:
            case SDLK_e:
                keys[K_STEP_B5] = state;
                break;
            case SDLK_KP_2:
            case SDLK_DOWN:
            case SDLK_d:
                keys[K_STEP_F5] = state;
                break;
            case SDLK_LSHIFT:
            case SDLK_RSHIFT:
                keys[K_FAST] = state;
                break;
            case SDLK_LCTRL:
            case SDLK_RCTRL:
                keys[K_JUMP_SELECT] = state;
                break;
            case SDLK_SPACE:
            case SDLK_KP_0:
                if (txtent) break;
                if (!pick_imm && !!state != !!keys[K_SELECT]) {
                    keys[K_SELECT] = state;
                    selectval = imglist[curimg].select ^ 1;
                }
                break;
            case SDLK_KP_PLUS:
            case SDLK_KP_MINUS:
                if (txtent) break;
                selectval = (event.key.keysym.sym == SDLK_KP_PLUS);
                keys[K_SELECT] = state;
                break;
            }
        }


        switch (event.type) {
        case SDL_KEYDOWN:
            switch (event.key.keysym.sym) {
            case SDLK_1: if (!txtent) Rotate(0); Refresh(); break;
            case SDLK_2: if (!txtent) Rotate(1); Refresh(); break;
            case SDLK_3: if (!txtent) Rotate(2); Refresh(); break;
            case SDLK_4: if (!txtent) Rotate(3); Refresh(); break;
            case SDLK_TAB:
                showinfo ^= 1;
                Refresh();
                break;
            case SDLK_KP_9:
            case SDLK_PAGEUP:
            case SDLK_w:
                if (!txtent)
                    ChangeImage(-1, keys[K_FAST] ? 100 : 20);
                break;
            case SDLK_KP_3:
            case SDLK_PAGEDOWN:
            case SDLK_r:
                if (!txtent)
                    ChangeImage(1, keys[K_FAST] ? 100 : 20);
                break;

            case SDLK_KP_7:
            case SDLK_HOME:
                if (!txtent)
                    newimg = 0;
                Refresh();
                break;
            case SDLK_KP_1:
            case SDLK_END:
                if (!txtent)
                    newimg = imglist.size()-1;
                Refresh();
                break;
            case SDLK_RETURN:
            case SDLK_KP_ENTER:
            {
                BeginTextEntry();
            }
            break;
            case SDLK_g:
            {
                Image& cimg(imglist[curimg]);
                printf("%s\t\n", cimg.name.c_str());
                fflush(stdout);
            }
            break;
            case SDLK_ESCAPE:
            case SDLK_q:
                if (!txtent)
                    throw 0;

            default:
                break;
            }
            break;
        case SDL_MOUSEBUTTONDOWN:
            if (txtent) break;
            switch (event.button.button) {
            case 5:
                ChangeImage(1, 1);
                break;
            case 4:
                ChangeImage(-1, 1);
                break;
            }
            break;
        case SDL_QUIT:
            throw 0;
            break;
        default:
            break;
        }
        CheckKeys();
    }
};

class RootComponent : public Component {
public:
    bool dirty;
    RootComponent() : dirty(true) { }
    void Refresh() {
        dirty = true;
    }
    //bool Event(SDL_Event& evt) { return false; }
};

#ifdef unix
string pjoin(string path1, string path2) {
    if (path2[0] == '/')
        return path2;
    if (path1.empty() || path1[path1.size()-1] == '/')
        return path1 + path2;
    return path1 + "/" + path2;
}

#endif


bool CanLoad(string path) {
    static DECLSPEC int SDLCALL(*testers[])(SDL_RWops*) = {
        IMG_isJPG,
        IMG_isPNG,
        IMG_isGIF,
        IMG_isBMP,
        IMG_isPNM,
        IMG_isXPM,
        //IMG_isXCF,
        IMG_isPCX,
        IMG_isTIF,
        IMG_isLBM,
        NULL
    };
    SDL_RWops* rw = SDL_RWFromFile(path.c_str(), "r");
    if (!rw) return false;
    for (int i = 0; testers[i]; i++) {
        SDL_RWseek(rw, 0, SEEK_SET);
        if (testers[i](rw)) {
            SDL_RWclose(rw);
            putchar('!'); fflush(stdout);
            return true;
        }
        putchar('.'); fflush(stdout);
    }
    SDL_RWclose(rw);
    return false;

}

bool isdir(string path) {
    struct stat st;

    if (lstat(path.c_str(), &st))
        return false;

    return S_ISDIR(st.st_mode);
}

string GetFilename(string name, string thumbdir) {
    if (thumbdir.empty()) return name;
    int pos = name.rfind('/');
    if (pos == string::npos)
        return pjoin(thumbdir, name);
    return pjoin(pjoin(name.substr(0,pos), thumbdir), name.substr(pos+1));
}

static void AddFile(vector<Image>& imglist, string path, string thumbdir, bool recursive=false, bool nodirs=false) {
    if (!nodirs && isdir(path)) {
        DIR* d = opendir(path.c_str());
        struct dirent* de;
        vector<string> names;
        while ((de = readdir(d))) {
            names.push_back(de->d_name);
        }
        closedir(d);

        sort(names.begin(), names.end());
        if (recursive) {
            FOREACH(name, names) {
                if (*name == "." || *name == ".." || *name == thumbdir) continue;
                string newpath = pjoin(path, *name);
                if (isdir(newpath))
                    AddFile(imglist, newpath, thumbdir, true);
            }
        }
        FOREACH(name, names) {
            string newpath = pjoin(path, *name);
            if (!isdir(newpath)) {
                int epos = name->rfind('.');
                if (epos != string::npos) {
                    string ext = name->substr(epos+1);
                    FOREACH(c, ext) { *c = tolower(*c); }
                    if (ext == "jpg" || ext == "jpeg" ||
                        ext == "png" || ext == "bmp" ||
                        ext == "gif" || ext == "pcx" ||
                        ext == "xpm" || ext == "ppm" ||
                        ext == "pbm" || ext == "pgm" ||
                        ext == "xbm" || ext == "tif" ||
                        ext == "tiff" || ext == "lbm")
                        AddFile(imglist, newpath, thumbdir);
                }
            }
        }
    } else {
        string thumbpath = GetFilename(path, thumbdir);
        imglist.push_back(Image(path,thumbpath));
    }
}

#ifndef DEFW
#define DEFW 1280
#endif

#ifndef DEFH
#define DEFH 960
#endif

int main(int argc, char *argv[]) {
    int curimg, retval;
    SDL_Event event;
    string firstimg;
    string thumbdir;
    string entry_text;
    FILE* f;
    int waittime = -1;
    bool fs = true;
    bool nodirs = false;
    bool pick_imm = false;
    bool initial_entry = false;
    bool loop = false;
    bool showinfo = false;
    char* pickfile=NULL;
    Uint8* imgpick=NULL;

    vector<Image> imglist;

    setenv("SDL_VIDEO_MINIMIZE_ON_FOCUS_LOSS", "0", 0);

    /* Check command line usage */
    if (!argv[1]) {
        cerr << "Usage: "<<argv[0]<<" <image_file>" <<endl;
        return 1;
    }
    maxsize = 4096;


    screenw = DEFW;
    screenh = DEFH;

    for (int i = 1; i < argc; i++) {
        if (*argv[i] == '-') {
            if (!strcmp(argv[i], "-f")) {
                firstimg = argv[++i];
            } else if (!strcmp(argv[i], "-t")) {
                thumbdir = argv[++i];
            } else if (!strcmp(argv[i], "-m")) {
                maxsize = atoi(argv[++i]);
            } else if (!strcmp(argv[i], "-r")) {
                AddFile(imglist, argv[++i], thumbdir, true, nodirs);
            } else if (!strcmp(argv[i], "-n")) {
                fs = false;
            } else if (!strcmp(argv[i], "-l")) {
                loop = true;
            } else if (!strcmp(argv[i], "-i")) {
                showinfo = true;
            } else if (!strcmp(argv[i], "-d")) {
                nodirs = true;
            } else if (!strcmp(argv[i], "-g")) {
                int ww, hh;

                if (sscanf(argv[++i], "%dx%d", &ww, &hh)==2) {
                    screenw=ww;screenh=hh;
                }
            } else if (!strcmp(argv[i], "-p")) {
                pickfile = argv[++i];
            } else if (!strcmp(argv[i], "-w")) {
                waittime = SDL_GetTicks() + atoi(argv[++i]);
            } else if (!strcmp(argv[i], "-s")) {
                char str[256];
                str[255] = 0;
                i++;
                if (!strcmp(argv[i], "-")) f=stdin;
                else f = fopen(argv[i], "r");
                if (f != NULL) {
                    while (fgets(str, sizeof(str)-1, f)) {
                        chomp(str);
                        if (*str)
                            AddFile(imglist, str, thumbdir, false, nodirs);
                    }
                    if (f != stdin) fclose(f);
                }
            } else if (!strcmp(argv[i], "--entry")) {
                initial_entry = true;
            } else if (!strcmp(argv[i], "--entry-text")) {
                initial_entry = true;
                entry_text = argv[++i];
            }
        } else {
            AddFile(imglist, argv[i], thumbdir);
        }
    }
    if (imglist.empty()) {
        cerr << "Could not load any images!"<<endl;
        return 1;
    }
    /* Initialize the SDL library */
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        cerr << "Couldn't initialize SDL: " << SDL_GetError() <<endl;
        return 255;
    }
    InitGL(screenw, screenh, fs);
    curimg=0;

    bool valid = false;

    int glmaxsize = 0;
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &glmaxsize);
    if (maxsize > glmaxsize) maxsize = glmaxsize;
    //cerr << "max texture size is " << maxsize << endl;
    glGenTextures(1, (GLuint*)&pictex);
    glBindTexture(GL_TEXTURE_2D, pictex);
    AllocTexture(maxsize, maxsize, 0);

    Font::default_font = &font_default;


    curimg = 0;
    if (!firstimg.empty() || pickfile) {
        map<string, int> smap;
        for (int i = 0; i < imglist.size(); i++) {
            smap[imglist[i].name] = i;
        }

        if (pickfile) {
            FILE* f;
            if (!strcmp(pickfile, "--")) {
                pick_imm = true;
                pickfile = NULL;
            } else if (strcmp(pickfile, "-")) {
                f = fopen(pickfile, "r");
                if (f) {
                    char str[256];
                    while (fgets(str, 256, f)) {
                        chomp(str);

                        map<string, int>::iterator itr = smap.find(string(str));
                        if (itr != smap.end()) imglist[itr->second].select = true;
                    }
                    fclose(f);
                }
            }
        }
        map<string, int>::iterator itr = smap.find(firstimg);
        if (itr == smap.end()) {
            curimg = atoi(firstimg.c_str());
            if (curimg < 0) curimg += imglist.size();
            if (curimg > imglist.size()) curimg = 0;
        } else {
            curimg = itr->second;
        }
    }

    RootComponent root;
    root.MakeStatic();

    ImageComponent* ic = new ImageComponent(imglist, curimg, pick_imm, loop, showinfo);
    root.Add(ic);
    ic->GrabFocus();

    if (initial_entry) {
        ic->BeginTextEntry(entry_text);
    }

    try {
        while (1) {
            if (waittime)
                if (SDL_GetTicks() >= waittime) {
                    throw 0;
                }
            while ( SDL_PollEvent(&event) ) {
                ic->Event(event);
            }
            if (root.dirty) {
                root.dirty = false;
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

                glViewport(0, 0, real_screenw, real_screenh);

                glMatrixMode(GL_MODELVIEW);
                glLoadIdentity();

                glMatrixMode(GL_PROJECTION);
                glLoadIdentity();
                RotateOrtho (0, screenw, screenh, 0);

                root.Render(0, 0);
                CHECKERROR();
                SDL_GL_SwapWindow(mainwindow);
            } else {
                SDL_Delay(10);
                ic->Idle();
                if (initial_entry && !ic->EnteringText())
                    break;
            }
        }
    } catch (int i) {
        retval = i;
    }
    if (!pickfile || !strcmp(pickfile, "-")) {
        f = stdout;
    } else {
        f = fopen(pickfile, "w");
    }
    FOREACH(img, imglist) {
        if (img->select) {
            fprintf(f, "%s\n", img->name.c_str());
        }
    }
    if (f != stdout) fclose(f);

    SDL_Quit();
    return 0;
}
