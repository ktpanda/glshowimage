
#include "config.h"

#include "common.h"
#include "text.h"
#include "util.h"

Font* Font::default_font;

static inline Uint16 utf8_to_uni(const char*& utf8) {
    Uint16 ch = *utf8;
    if (ch == 0) return 0;
    if ( ch >= 0xF0 ) {
        ch  =  (Uint16)(ch&0x07) << 18;
        ch |=  (Uint16)(*++utf8 & 0x3F) << 12;
        ch |=  (Uint16)(*++utf8 & 0x3F) << 6;
        ch |=  (Uint16)(*++utf8 & 0x3F);
    } else if ( ch >= 0xE0 ) {
        ch  =  (Uint16)(ch & 0x1F) << 12;
        ch |=  (Uint16)(*++utf8 & 0x3F) << 6;
        ch |=  (Uint16)(*++utf8 & 0x3F);
    } else if ( ch >= 0xC0 ) {
        ch  =  (Uint16)(ch & 0x3F) << 6;
        ch |=  (Uint16)(*++utf8&0x3F);
    }
    utf8++;
    return ch;
}

static Uint16 *UTF8_to_UNICODE(Uint16 *unicode, const char *utf8, int len) {
    int i, j;
    Uint16 ch;

    for ( i=0, j=0; i < len; ++i, ++j ) {
        unicode[j] = utf8_to_uni(utf8);
    }
    unicode[j] = 0;

    return unicode;
}

Font::Font(unsigned char* imgdat, int imgdatlen, Glyph* glyphs,
         int nglyphs, int ascent, int descent) :
    tex(0), imgdat(imgdat),  imgdatlen(imgdatlen), glyphs(glyphs),
    nglyphs(nglyphs), ascent(ascent), descent(descent) { }

int Font::RenderText(const char* txt, int sx, int sy) {
    int cx=0;
    double tcxl,tcyl,tcxh,tcyh;

    if (!tex) {
        if (!imgdat) return 0;

        SDL_RWops* rw = SDL_RWFromMem(imgdat, imgdatlen);

        SDL_Surface* tmp = IMG_Load_RW(rw, 1);

        imgdat = NULL;

        if (!tmp) return 0;

        glGenTextures (1, (GLuint*)&tex);

        tw=powerof2(tmp->w);
        th=powerof2(tmp->h);

        glBindTexture(GL_TEXTURE_2D, tex);
        AllocTexture(tw, th, 0);
        UploadSurfaceToTexture(tmp);

        SDL_FreeSurface (tmp);
    }

    Uint16 c;
    const Glyph* g;
    glBindTexture(GL_TEXTURE_2D, tex);

    while(c = utf8_to_uni(txt)) {
        if (c >= nglyphs) continue;
        g = &glyphs[c];

        double tcxl,tcyl,tcxh,tcyh;
        tcxl=(double)g->sx / tw;
        tcxh=(double)(g->sx + g->sw) / tw;
        tcyl=(double)g->sy / th;
        tcyh=(double)(g->sy + g->sh) / th;

        glBegin(GL_QUADS);

        glTexCoord2d(tcxl,tcyl);
        glVertex2i(cx + sx + g->ox, sy + g->oy);

        glTexCoord2d(tcxh,tcyl);
        glVertex2i(cx + sx + g->ox + g->sw, sy + g->oy);

        glTexCoord2d(tcxh,tcyh);
        glVertex2i(cx + sx + g->ox + g->sw, sy + g->oy + g->sh);

        glTexCoord2d(tcxl,tcyh);
        glVertex2i(cx + sx + g->ox, sy + g->oy + g->sh);

        glEnd();

        cx += g->adv;
    }

    return cx;
}

int Font::MeasureText(const char* txt, int* pminx, int* pmaxx, int* pminy, int* pmaxy) const {
    int cx=0;
    int minx=0;
    int maxx=0;
    int miny=0;
    int maxy=0;
    int f=1;
    Uint16 c;
    int cminx,cminy,cmaxx,cmaxy;
    const Glyph* g;

    while(c = utf8_to_uni(txt)) {
        if (c >= nglyphs) continue;

        g = &glyphs[c];

        cminx=cx + g->ox;
        cminy=g->oy;
        cmaxx=cminx + g->sw;
        cmaxy=cminy + g->sh;
        if (f || minx > cminx) minx = cminx;
        if (f || maxx < cmaxx) maxx = cmaxx;
        if (f || miny > cminy) miny = cminy;
        if (f || maxy < cmaxy) maxy = cmaxy;
        f=0;

        cx += g->adv;
    }
    if (pminx) *pminx = minx;
    if (pmaxx) *pmaxx = maxx;
    if (pminy) *pminy = miny;
    if (pmaxy) *pmaxy = maxy;

    return cx;
}

void TextLayout::checktext() {
    splitlines(strm.str(), cfont, ccolor);
    strm.str("");
}

void TextLayout::Render(int bx, int by) {
    int cx, sy;

    checktext();

    sy = by - lines[0].ascent;

    FOREACH(cline, lines) {
        sy += cline->ascent;
        cx = 0;
        FOREACH(cchunk, cline->chunks) {
            cchunk->color.Set();
            if (cx < cchunk->tab) cx = cchunk->tab;
            cx += cchunk->font->RenderText(cchunk->txt.c_str(), cx + bx, sy);
        }

        sy += cline->descent;
    }

}
void TextLayout::Measure(int* pminx, int* pmaxx, int* pminy, int* pmaxy) {
    int minx = 0, maxx = 0, miny = 0, maxy = 0;
    int tminx, tmaxx, tminy, tmaxy;
    int sx, sy;
    bool f = true;

    checktext();

    sy = -lines[0].ascent;

    FOREACH(cline, lines) {
        sx = 0;
        sy += cline->ascent;
        FOREACH(cchunk, cline->chunks) {
            if (sx < cchunk->tab) sx = cchunk->tab;

            int adv = cchunk->font->MeasureText(cchunk->txt.c_str(),
                                                &tminx, &tmaxx, &tminy, &tmaxy);

            if (f || minx > (tminx + sx)) minx = (tminx + sx);
            if (f || maxx < (tmaxx + sx)) maxx = (tmaxx + sx);
            if (f || miny > (tminy + sy)) miny = (tminy + sy);
            if (f || maxy < (tmaxy + sy)) maxy = (tmaxy + sy);
            f = false;

            sx += adv;
        }

        sy += cline->descent;

    }

    if (pminx) *pminx = minx;
    if (pmaxx) *pmaxx = maxx;
    if (pminy) *pminy = miny;
    if (pmaxy) *pmaxy = maxy;
}

void TextLayout::addchunk(const string& txt, Font* font, Color color) {
    if (txt.empty() && !horiz_tab) return;


    TextLine& cline(*(lines.end()-1));
    if (cline.chunks.empty())
        cline.ascent = cline.descent = 0; // clear newline font size

    cline.chunks.push_back(TextChunk(txt, font, color, horiz_tab));

    horiz_tab = 0;

    if (cline.ascent < font->ascent) cline.ascent = font->ascent;
    if (cline.descent < font->descent) cline.descent = font->descent;
}

void TextLayout::splitlines(const string& text, Font* font, Color color) {
    int p, lp;

    if (text.empty()) return;
    lp = 0;
    while ((p = text.find('\n', lp)) != string::npos) {
        addchunk(text.substr(lp, p - lp), font, color);
        lp = p + 1;

        TextLine& cline(*(lines.end()-1));

        if (cline.chunks.empty()) {
            cline.descent = font->descent;
            cline.ascent = font->ascent;
        }

        lines.resize(lines.size()+1);

        TextLine& nline(*(lines.end()-1));

        nline.descent = font->descent;
        nline.ascent = font->ascent;
    }

    addchunk(text.substr(lp), font, color);

}

TextLayout::TextLayout(Font* font, Color color) : cfont(font), ccolor(color) {
    lines.resize(lines.size()+1);
}
